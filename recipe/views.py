from django.shortcuts import render, redirect,  get_object_or_404
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.urls import reverse, reverse_lazy
from django.contrib.auth.views import PasswordChangeView
from recipe.forms import RecipeForm
from recipe.models import Recipe, Note
from django.http import HttpResponse
from .models import Recipe, Profile
from .forms import ProfileUpdateForm, PasswordUpdateForm, CustomPasswordChangeForm, RegistrationForm
from recipe.serializers import NoteSerializer
from rest_framework import generics, permissions
from recipe.serializers import NoteSerializer

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .serializers import NoteSerializer


class ChangePasswordView(PasswordChangeView):
    template_name = 'change_password.html'
    success_url = reverse_lazy('recipe:profile')

def home(request):
    recipes = Recipe.objects.all()
    return render(request, 'home.html', {'recipes': recipes})

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']

            if User.objects.filter(username=username).exists():
                message = 'Username is not available. Please choose another username.'
                return render(request, 'register.html', {'form': form, 'message': message})

            user = User.objects.create_user(username=username, password=password, first_name=first_name, last_name=last_name)

            profile = Profile(user=user)
            profile.save()

            return redirect('recipe:home')
    else:
        form = RegistrationForm()

    return render(request, 'register.html', {'form': form})


def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            auth_login(request, user)
            return redirect(reverse('recipe:home'))
        else:
            messages.error(request, 'Invalid login credentials. Please try again.')

    return render(request, 'login.html')



def profile(request):
    user = request.user  

    context = {
        'user': user
    }

    return render(request, 'profile.html', context)

def logoff(request):
    logout(request)
    return redirect('recipe:home')


def add_recipe(request):
    if request.method == 'POST':
        form = RecipeForm(request.POST, request.FILES)
        if form.is_valid():
            recipe = form.save(commit=False)
            recipe.user = request.user
            recipe.save()
            return redirect('recipe:home')
    else:
        form = RecipeForm()
    return render(request, 'add_recipe.html', {'form': form})



def recipe_detail(request, recipe_id):
    recipe = get_object_or_404(Recipe, id=recipe_id)
    return render(request, 'recipe_detail.html', {'recipe': recipe})


def notes(request):
    notes = Note.objects.all()
    return render(request, 'notes.html', {'notes': notes})


class NoteListCreateAPIView(generics.ListCreateAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class NoteRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    

def delete_recipe(request, recipe_id):
    recipe = Recipe.objects.get(id=recipe_id)
    recipe.delete()
    return redirect('recipe:home')

def update_profile(request):
    if request.method == 'POST':
        form = ProfileUpdateForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('recipe:profile')
    else:
        form = ProfileUpdateForm(instance=request.user)
    
    return render(request, 'update_profile.html', {'form': form})


def about(request):
    return render(request, 'about.html')


class NoteCreateAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = NoteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)
    
    
@login_required
def create_note(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            note = form.save(commit=False)
            note.user = request.user  
            note.save()
            return redirect('recipe:notes')
    else:
        form = NoteForm()

    return render(request, 'create_note.html', {'form': form})


def search_recipes(request):
    query = request.GET.get('search_query')
    if query:
        recipes = Recipe.objects.filter(name__icontains=query)
    else:
        recipes = Recipe.objects.all()
    
    context = {'recipes': recipes, 'query': query}
    return render(request, 'home.html', context)

from django.db.models import Q
import re

def search_by_ingredient(request):
    query = request.GET.get('ingredient_query')
    if query:
        ingredients = query.split()
        query_parts = [f'(?i)(?=.*\\b{re.escape(ingredient)}\\b)' for ingredient in ingredients]
        query_regex = ''.join(query_parts)
        recipes = Recipe.objects.filter(Q(ingredients__iregex=query_regex))
    else:
        recipes = Recipe.objects.all()

    context = {'recipes': recipes, 'query': query}
    return render(request, 'search_by_ingredient.html', context)



