from django.urls import path
from . import views
from recipe.views import recipe_detail
from recipe.api.views import NoteListCreateAPIView
from django.conf import settings
from django.conf.urls.static import static
from .views import delete_recipe, ChangePasswordView, NoteCreateAPIView, search_recipes, search_by_ingredient

app_name = 'recipe'

urlpatterns = [
    
    path('', views.home, name='home'),
    path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
    path('profile/', views.profile, name='profile'),
    path('logoff/', views.logoff, name='logoff'),
    path('add-recipe/', views.add_recipe, name='add_recipe'),
    path('recipe/<int:recipe_id>/', views.recipe_detail, name='recipe_detail'),
    path('notes/', NoteListCreateAPIView.as_view(), name='notes'),
    path('notes/<int:pk>/', views.NoteRetrieveUpdateDestroyAPIView.as_view(), name='note_detail'),
    path('recipe/delete/<int:recipe_id>/', delete_recipe, name='delete-recipe'),
    path('profile/update/', views.update_profile, name='update_profile'),
    path('about/', views.about, name='about'),
    path('change-password/', ChangePasswordView.as_view(), name='change_password'),
    path('api/notes/', NoteCreateAPIView.as_view(), name='create-note'),
    path('notes/create/', views.create_note, name='create_note'),
    path('search/', views.search_recipes, name='search'),
    path('search-by-ingredient/', search_by_ingredient, name='search_by_ingredient'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
